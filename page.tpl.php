<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>

    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>

  </head>
  <body>
    <!-- wrapper start -->
    <div id="wrapper"><div id="bottom_frame"><div id="top_frame">
    <!-- begin container -->
    <div id="container">

      <!-- begin header -->
      <div id="header">
        <div class="wrapper">

           <!-- logo -->
		  <?php if ($logo) : ?>
        <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
          <img class="logo" src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
        </a>
        <a href="<?php print $base_path ?>rss.xml"><img class="rss" src="<?php print $base_path ?>sites/all/themes/gardening/images/spacer.gif" alt="feed" width="149" height="111" /></a>
        <div id="search-bloc" class="container-inline"><?php print $search_box ?></div>
      <?php endif; ?><!-- end site logo -->
		  <?php if ($site_name) { ?><h2 class='siteName'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h2><?php } ?>
          <?php if ($site_slogan) { ?><h2 class='siteSlogan'><?php print $site_slogan ?></h2><?php } ?>

        </div><!-- end header wrapper -->

        <!-- begin navbar -->
        <div id="navbar">
          <div class="wrapper">

            <?php if (isset($primary_links)) : ?>
              <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navtabs')) ?>
            <?php endif; ?>

          </div><!-- end navbar wrapper -->
        </div><!-- end navbar -->


      </div><!-- end header -->

        <!-- begin mainContent -->
        <div id="mainContent">
          <div class="wrapper">

            <!-- begin contentLeft -->
            <div class="contentLeft">
              <?php if ($mission): print '<div class="mission">'. $mission .'</div>'; endif; ?>
              <?php if ($breadcrumb): print '<div class="breadcrumb">'. $breadcrumb . '</div>'; endif; ?>
              <?php if ($title) : print '<h2 class="pageTitle">' . $title . '</h2>'; endif; ?>
              <?php if ($tabs) : print '<div class="tabs">' . $tabs . '</div>'; endif; ?>
              <?php if ($help) : print '<div class="help">' . $help . '</div>'; endif; ?>
              <?php if ($messages) : print '<div class="messages">' .$messages . '</div>'; endif; ?>
              <?php print $content_top; ?>
              <?php print $content; ?>
              <?php print $content_bottom; ?>
              <?php print $feed_icons; ?>
            </div><!-- end contentLeft -->

              <!-- begin sidebar -->
              <?php if ($sidebar) : ?>
                <div id="sidebar"><div id="welcome"><div><?php include("welcome.php"); ?></div></div>
                  <?php print $sidebar; ?>
                </div>
              <?php endif; ?>
              <!-- end sidebar -->

            <div class="clear"></div>

          </div><!--end mainContent wrapper-->
        </div><!-- end mainContent -->
        <!-- begin footer -->
        <div id="footer">
          <div class="wrapper">

           </div><!-- end footer wrapper -->
          <div class="clear"></div>


        </div><!-- end footer -->
        <div class="footerMessage">
          <?php print $footer_message; ?> - Designed by <a href="http://www.ezwpthemes.com/">EZwpthemes </a> Drupalized by <a href="http://www.azridesign.com/">AzriDesign </a>
        </div>

      </div><!-- end contentWrapper -->
      </div></div></div><!-- wrapper end -->
    <?php print $closure ?>
  </body>
</html>
